const Course = require("../model/Course");

module.exports.addCourse = async (userData,reqBody) => {

	if(userData.isAdmin == true){
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	// Save the created object to the database
	return newCourse.save().then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}

	})
}

	else{
		return "You dont have authorization";
	}

}

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = async (userData,reqParams,reqBody) => {
	if(userData.isAdmin){
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}

	})
	}
	else{
		return "You dont have authorization";
	}
}


//Activity S35
module.exports.archiveCourse = async (userData,reqParams,reqBody) => {
	if(userData.isAdmin){
		console.log(reqBody.isActive);
		let archiveCourse = {
		isActive : reqBody.isActive }
	
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}

	})
	}

	else{
		return "You dont have authorization";
	}
}