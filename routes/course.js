const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");


router.post("/", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);
	courseController.addCourse(userData,req.body).then(resultFromController => res.send(
	resultFromController));
})

router.get("/all",(req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(
	resultFromController));
})

router.get("/",(req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(
	resultFromController));
})

router.get("/:courseId",(req,res)=>{
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(
	resultFromController));
})

router.put("/:courseId", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	courseController.updateCourse(userData,req.params,req.body).then(resultFromController => res.send(
	resultFromController));
})

router.put("/:courseId/archive", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	courseController.archiveCourse(userData,req.params,req.body).then(resultFromController => res.send(
	resultFromController));
})

module.exports = router;

