const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//route for registering a user
router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));
})

//routes for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//routes for authenticating a user
router.post("/details",(req,res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(
		resultFromController));
})

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



router.post("/enroll", auth.verify,(req,res) => {
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	const userData = auth.decode(req.headers.authorization);
	userController.enroll(userData,data).then(resultFromController => res.send(
	resultFromController));
})

// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;