const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");
const app = express();



app.use(express.json());
app.use(express.urlencoded({extended: true}));

//to connect to mongoDB Atlas
mongoose.connect("mongodb+srv://devjeff:jeffjeff@wdc028-course-booking.o1lar.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		//to avoid error in conneting to mongoDB atlas
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

//to check if we are connected to MongoDB
mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas'));


app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

//environment variable port or port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});

